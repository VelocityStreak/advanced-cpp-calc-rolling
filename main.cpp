#include <cstdlib>
#include <iostream>
#include <functional>
#include <istream>
#include <math.h>
#include <memory.h>
#include <tuple>
#include <string>

#define log(x) std::cout << x << std::endl;

using std::cout, std::cin;

auto multiply(int n1, int n2) {
    struct result {int answer;};
    return result {n1 * n2};
}

auto divide(int n1, int n2) {
    struct result {int answer; int remainder;};
    return result {n1 /n2, n1 % n2};
}

int launch() {
    cout << "What would you like to do: [multiply, divide, quit]:\t\n";

    std::string selection;

    cin >> selection;

    if(selection.compare("multiply") != selection.compare("divide")) {
        cout << "Enter 2 numbers to multiply:\n";
        auto num1 = 0;
        auto num2 = 0;

        cin >> num1;
        cin >> num2;

        auto result = multiply(num1, num2);

        cout << "Answer:\t\t" << result.answer << "\n";

    }

    if (selection.compare("divide") != selection.compare("multiply")) {
        cout << "Enter 2 numbers to divide (order matters!):\n";
        auto num1 = 0;
        auto num2 = 0;

        cin >> num1;
        cin >> num2;

        auto result = divide(num1, num2);

        cout << "Answer:\t\t" << result.answer << "\n" << "Remainder[s]:\t" << result.remainder << '\n';

    }

    return EXIT_SUCCESS;
}

int main() {

    launch();

    return 0;
}